import torch
from torch.utils.data import Dataset
import numpy as np
import itertools


class WSJTestDataset(Dataset):
    def __init__(self, data):
        """
        Args:
            data (string): Path to test data
        """
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return torch.as_tensor(self.data[idx]), torch.as_tensor(self.data[idx].shape[0])


class TestPadSequence:
    def __call__(self, batch):
        seqs = [item[0] for item in batch]
        lengths = [item[1] for item in batch]

        padded_seqs = torch.nn.utils.rnn.pad_sequence(seqs, batch_first=True)
        
        return self.sort_batch(padded_seqs, torch.as_tensor(lengths))

    def sort_batch(self, batch, lengths):
        seq_lengths, perm_idx = lengths.sort(0, descending=True)
        seq_tensor = batch[perm_idx]
        return seq_tensor, seq_lengths, perm_idx
