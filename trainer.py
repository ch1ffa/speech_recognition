import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils, datasets
import ctcdecode
import time
import datetime
import copy
import numpy as np

from tensorboardcolab import TensorBoardColab

from .utils import AverageMeter, AvgDistanceMeter

PHONEME_MAP = [
    '=',  # blank
    '_',  # "+BREATH+"
    '+',  # "+COUGH+"
    '~',  # "+NOISE+"
    '!',  # "+SMACK+"
    '-',  # "+UH+"
    '@',  # "+UM+"
    'a',  # "AA"
    'A',  # "AE"
    'h',  # "AH"
    'o',  # "AO"
    'w',  # "AW"
    'y',  # "AY"
    'b',  # "B"
    'c',  # "CH"
    'd',  # "D"
    'D',  # "DH"
    'e',  # "EH"
    'r',  # "ER"
    'E',  # "EY"
    'f',  # "F"
    'g',  # "G"
    'H',  # "HH"
    'i',  # "IH"
    'I',  # "IY"
    'j',  # "JH"
    'k',  # "K"
    'l',  # "L"
    'm',  # "M"
    'n',  # "N"
    'G',  # "NG"
    'O',  # "OW"
    'Y',  # "OY"
    'p',  # "P"
    'R',  # "R"
    's',  # "S"
    'S',  # "SH"
    '.',  # "SIL"
    't',  # "T"
    'T',  # "TH"
    'u',  # "UH"
    'U',  # "UW"
    'v',  # "V"
    'W',  # "W"
    '?',  # "Y"
    'z',  # "Z"
    'Z',  # "ZH"
]


class Trainer:

    def __init__(self, config):
        self.cuda = config.cuda
        self.device = config.device
        self.seed = config.seed
        self.learning_rate = config.lr
        self.weight_decay = config.weight_decay
        self.epochs = config.epochs
        self.batch_size = config.batch_size
        self.log_interval = config.log_interval

        self.train_loader = config.train_loader
        self.test_loader = config.test_loader

        self.model = config.model

        self.globaliter = 0
        self.tbc = None

        torch.manual_seed(self.seed)

        self.optimizer = optim.Adam(
            self.model.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=5, gamma=0.1)
        self.criterion = nn.CTCLoss()

        self.vocab = PHONEME_MAP

        self.init_time = datetime.datetime.now().strftime("%d_%m_%y_%I_%M")
        self.best_distance = 100

    def train_epoch(self):
        self.model.train()
        losses = AverageMeter()
        start_time = time.time()
        for batch_idx, (inputs, targets, input_sizes, target_sizes) in enumerate(self.train_loader):
            self.globaliter += 1
            inputs, input_sizes = inputs.to(
                self.device), input_sizes.to(self.device)

            targets, target_sizes = targets.to(
                self.device), target_sizes.to(self.device)

            self.optimizer.zero_grad()

            output, input_sizes = self.model(inputs, input_sizes)
            loss = self.criterion(
                output, targets, input_sizes, target_sizes).to(self.device)
            loss = loss / inputs.size(0)
            losses.update(loss.item(), inputs.size(0))

            loss.backward()
            self.optimizer.step()

            if batch_idx % self.log_interval == 0:
                self.tbc.save_value('Train Loss', 'train_loss',
                                    self.globaliter, losses.val)

        train_time = time.time() - start_time

        return losses.avg, train_time

    def test(self):
        self.model.eval()

        start_time = time.time()
        decoder = ctcdecode.CTCBeamDecoder(self.vocab)
        losses = AverageMeter()
        dis = AvgDistanceMeter(self.vocab)

        with torch.no_grad():
            for inputs, targets, input_sizes, target_sizes in self.test_loader:
                inputs, input_sizes = inputs.to(
                    self.device), input_sizes.to(self.device)

                targets, target_sizes = targets.to(
                    self.device), target_sizes.to(self.device)

                output, lengths = self.model(inputs, input_sizes)

                beam_result, _, _, out_seq_len = decoder.decode(
                    output.exp().transpose(0, 1), lengths)

                dis(beam_result, targets, out_seq_len, target_sizes)

                loss = self.criterion(
                    output, targets, lengths, target_sizes).to(self.device)
                loss = loss / inputs.size(0)
                losses.update(loss.item(), inputs.size(0))
                
        val_time = time.time() - start_time

        return losses.avg, val_time, dis.avg

    def train(self, start_epoch, epochs):
        self.tbc = TensorBoardColab()
        best_model_wts = copy.deepcopy(self.model.state_dict())

        for epoch in range(start_epoch, epochs):

            train_loss, train_time = self.train_epoch()
            print(f'Training Summary Epoch: [{epoch + 1}]\t'
                  f'Time taken: {train_time:.0f} s\t'
                  f'Average Loss {train_loss:.4f}\t')

            test_loss, test_time, avg_distance = self.test()

            if avg_distance < self.best_distance:
                self.best_distance = avg_distance
                best_model_wts = copy.deepcopy(self.model.state_dict())
                self.save_checkpoint()

            print(f'Validation Summary:\t'
                  f'Time taken (s): {test_time:.0f}\t'
                  f'Average Loss {test_loss:.3f}\t'
                  f'Average distance: {avg_distance:.3f}\t')

            self.scheduler.step()

        self.model.load_state_dict(best_model_wts)

    def save_checkpoint(self):
        p ='/gdrive/My Drive/checkpoints/best_' + self.init_time
        torch.save({
            'model': self.model.state_dict(), 
            'optimizer': self.optimizer.state_dict(),
            'scheduler': self.scheduler.state_dict(),
            'best_distance': self.best_distance
        }, p)

    def load_checkpoint(self, path):
        checkpoint = torch.load(path)
        if 'model' in checkpoint: 
            self.model.load_state_dict(checkpoint['model'])
        if 'optimizer' in checkpoint: 
            self.optimizer.load_state_dict(checkpoint['optimizer'])
        if 'scheduler' in checkpoint: 
            self.scheduler.load_state_dict(checkpoint['scheduler'])
        if 'best_distance' in checkpoint:
            self.best_distance = checkpoint['best_distance']

    def generate_test_results(self, test_loader):
        self.model.eval()

        start_time = time.time()
        decoder = ctcdecode.CTCBeamDecoder(self.vocab)

        results = []

        with torch.no_grad():
            for inputs, input_sizes, perm_idx in test_loader:
                inputs, input_sizes = inputs.to(
                    self.device), input_sizes.to(self.device)

                output, lengths = self.model(inputs, input_sizes)

                beam_result, _, _, out_seq_len = decoder.decode(
                    output.exp().transpose(0, 1), lengths)

                for i in np.argsort(perm_idx):
                    size = out_seq_len[i][0]
                    res = beam_result[i][0][0:size]
                    utterances = ''.join([self.vocab[x] for x in res])
                    results.append(utterances)

        test_time = time.time() - start_time

        print(f'Test Summary:\t'
              f'Time taken (s): {test_time:.0f}\t')

        return results
