import Levenshtein as Lev


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class AvgDistanceMeter(AverageMeter):
    def __init__(self, phoneme_map):
        super().__init__()
        self.phonemes = phoneme_map

    def __call__(self, *args, **kwargs):
        self.calc_distance(*args, **kwargs)

    def calc_distance(self, out, targets, seq_len, target_sizes):
        results = self.convert_to_strings(out, seq_len)
        s_targets = self.split_targets_and_convert(targets, target_sizes)

        for r, t in zip(results, s_targets):
            self.update(Lev.distance(r, t))

    def convert_to_strings(self, out, seq_len):
        results = []
        for b, batch in enumerate(out):
            size = seq_len[b][0]
            utterances = ''.join([self.phonemes[x] for x in batch[0][0:size]])
            results.append(utterances)
        return results

    def split_targets_and_convert(self, targets, target_sizes):
        result = []
        offset = 0
        for size in target_sizes:
            target_tensor = targets[offset:offset + size]
            target_string = ''.join([self.phonemes[x] for x in target_tensor])
            result.append(target_string)
            offset += size
        return result
