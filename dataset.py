import torch
from torch.utils.data import Dataset
import numpy as np
import itertools


class WSJDataset(Dataset):
    def __init__(self, data, targets):
        """
        Args:
            data (string): Path to train data
            labels (string): Path to train lables
        """
        self.data = data
        self.targets = targets

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return torch.as_tensor(self.data[idx]), torch.as_tensor(self.targets[idx] + 1), torch.as_tensor(self.data[idx].shape[0]), torch.as_tensor(len(self.targets[idx]))


class PadSequence:
    def __call__(self, batch):
        seqs = [item[0] for item in batch]
        targs = [item[1] for item in batch]
        lengths = [item[2] for item in batch]
        targs_lengths = [item[3] for item in batch]

        padded_seqs = torch.nn.utils.rnn.pad_sequence(seqs, batch_first=True)
        
        return self.sort_batch(padded_seqs, targs, torch.as_tensor(lengths), torch.as_tensor(targs_lengths))

    def sort_batch(self, batch, targets, lengths, target_lengths):
        seq_lengths, perm_idx = lengths.sort(0, descending=True)
        seq_tensor = batch[perm_idx]
        target_len_tensor = target_lengths[perm_idx]
        target_tensor = [targets[i] for i in perm_idx]
        target_tensor = torch.as_tensor(list(itertools.chain(*target_tensor)))
        return seq_tensor, target_tensor, seq_lengths, target_len_tensor
